package es.raulprieto.ficheros;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class EscribirExternaActivity extends AppCompatActivity {
    static final int REQUEST_WRITE = 1;
    static final int REQUEST_WRITE2 = 2;

    public static final String NOMBREFICHERO = "resultado.txt";
    public static final String CODIFICACION = "UTF-8";
    Memoria miMemoria;

    @BindView(R.id.edTexto)
    EditText edTexto;
    @BindView(R.id.tvPropiedades)
    TextView tvPropiedades;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_escribir_externa);
        ButterKnife.bind(this);

        miMemoria = new Memoria(getApplicationContext());
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_WRITE:
                if (tienePermiso())
                    escribir();
                else
                    Toast.makeText(this, "No hay permiso para escribir en la memoria externa", Toast.LENGTH_SHORT).show();
                break;
        }
    }

    @OnClick(R.id.btGuardar)
    public void guardar() {
        /**
         * Comprueba si tiene los permisos necesarios.
         * En caso negativo los solicita.
         */
        if (obtienePermiso() && puedeEscribir())
            escribir();
        ((InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE)).hideSoftInputFromWindow(edTexto.getWindowToken(),0);
    }

    private boolean puedeEscribir() {
        return miMemoria.disponibleEscritura();
    }

    private boolean tienePermiso() {
        return ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED;
    }

    private boolean obtienePermiso() {
        if (!tienePermiso()) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_WRITE);
            return false;
        }
        return true;
    }

    private void escribir() {
        if (miMemoria.escribirExterna(NOMBREFICHERO, edTexto.getText().toString(), false, CODIFICACION))
            tvPropiedades.setText(miMemoria.mostrarPropiedadesExterna(NOMBREFICHERO));
        else
            Toast.makeText(this, "No existe el fichero " + NOMBREFICHERO, Toast.LENGTH_SHORT).show();
    }
}
