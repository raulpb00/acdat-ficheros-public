package es.raulprieto.ficheros;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ExploradorActivity extends AppCompatActivity {
    private static final int OPEN_FILE_REQUEST_CODE = 1;
    private static final String CODIFICACION = "UTF-8";
    Memoria miMemoria;
    Resultado resultado;

    @BindView(R.id.tvRuta)
    TextView tvRuta;
    @BindView(R.id.tvContenido)
    TextView tvContenido;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_explorador);
        ButterKnife.bind(this);
        miMemoria = new Memoria(this);

        if (obtienePermiso(OPEN_FILE_REQUEST_CODE))
            Toast.makeText(this, "Permisos de lectura concedidos", Toast.LENGTH_SHORT).show();
        else
            Toast.makeText(this, "Faltan permisos", Toast.LENGTH_SHORT).show();
    }


    @OnClick(R.id.btAbrir)
    public void onClick(View v) {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("file/*");
        if (intent.resolveActivity(getPackageManager()) != null)
            startActivityForResult(intent, OPEN_FILE_REQUEST_CODE);
        else
            //informar que no hay ninguna aplicación para manejar ficheros
            Toast.makeText(this, "No hay aplicación para manejar ficheros", Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        String path, mensaje;
        mensaje = "";

        if (requestCode == OPEN_FILE_REQUEST_CODE)
            if (resultCode == RESULT_OK) {
                // Mostramos en la etiqueta la ruta del archivo seleccionado
                path = data.getData().getPath();
                if (path != null) {
                    tvRuta.setText(path);
                    mensaje = extraerDatosRutaExterna(path, tvContenido); // Extrae la información de la ruta y devuelve el mensaje de error.
                }
            } else
                mensaje = "Fichero no seleccionado";
        Toast.makeText(this, mensaje, Toast.LENGTH_SHORT).show();
    }

    private String extraerDatosRutaExterna(String fichero, TextView destino) {
        String mensaje;
        resultado = miMemoria.leerRutaExterna(fichero, CODIFICACION);
        if (resultado.getCodigo()) {
            destino.setText(resultado.getContenido());
            mensaje = "Fichero leído correctmaente: " + fichero;
        } else {
            destino.setText(" ");
            mensaje = "Error al leer: " + resultado.getMensaje();
        }
        return mensaje;
    }

    private boolean tienePermiso() {
        return ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED;
    }

    private boolean obtienePermiso(int requestType) {
        if (!tienePermiso()) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, requestType);
            return false;
        }
        return true;
    }
}
