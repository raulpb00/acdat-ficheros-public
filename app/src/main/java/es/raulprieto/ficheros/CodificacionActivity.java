package es.raulprieto.ficheros;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class CodificacionActivity extends AppCompatActivity {
    static final int REQUEST_WRITE_NUEVO = 1;

    Memoria miMemoria;
    Resultado resultado;
    @BindView(R.id.edDestino)
    EditText edDestino;
    @BindView(R.id.tvContenido)
    TextView tvContenido;
    @BindView(R.id.edNuevoNombre)
    EditText edNuevoNombre;
    @BindView(R.id.rgCodEscritura)
    RadioGroup rgCodEscritura;
    @BindView(R.id.rgCodLectura)
    RadioGroup rgCodLectura;

    @OnClick(R.id.btLeer)
    public void leer() {
        try{
            miMemoria = new Memoria(this);
            extraerDatosMExterna(edDestino.getText().toString(), tvContenido);
        }catch (Exception e){
            Toast.makeText(this, "No existe el fichero con ese nombre", Toast.LENGTH_SHORT).show();
        }
        finally {
            EsconderTeclados();
        }
    }

    @OnClick(R.id.btGuardar)
    public void guardar() {
        try{
            if (obtienePermiso(REQUEST_WRITE_NUEVO) && puedeEscribir())
                escribir(edNuevoNombre.getText().toString(), tvContenido.getText().toString(), false, getCodEscritura());
        }catch (Exception e){
            Toast.makeText(this, "No existe el fichero con ese nombre", Toast.LENGTH_SHORT).show();
        }
        finally {
            EsconderTeclados();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case REQUEST_WRITE_NUEVO:
                if (tienePermiso()) {
                    escribir(edNuevoNombre.getText().toString(), edDestino.getText().toString(), false, getCodEscritura());
                } else
                    Toast.makeText(this, "No hay permisos para escribir en la m. Externa", Toast.LENGTH_SHORT).show();
                break;
        }

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_codificacion);
        ButterKnife.bind(this);
    }

    /**
     * Extrae un dato de un <b>fichero externo</b> para
     * pintarlo en un <b>TextView o EditText</b> de destino
     *
     * @param fichero Fichero contenedor del dato
     * @param destino TextView de destino
     */
    private void extraerDatosMExterna(String fichero, TextView destino) {
        resultado = miMemoria.leerExterna(fichero, getCodLectura());
        if (resultado.getCodigo())
            destino.setText(resultado.getContenido());
        else {
            destino.setText("0");
            Toast.makeText(this, "Error al leer la m. Externa" + fichero, Toast.LENGTH_SHORT).show();
        }
    }

    private String getCodEscritura() {
        RadioButton rbSeleccionado = findViewById(rgCodEscritura.getCheckedRadioButtonId());
        return rbSeleccionado.getText().toString();
    }

    private String getCodLectura() {
        RadioButton rbSeleccionado = findViewById(rgCodLectura.getCheckedRadioButtonId());
        return rbSeleccionado.getText().toString();
    }

    private void escribir(String datoSd, String valor, boolean b, String codificacion) {
        String mensaje;
        if (miMemoria.escribirExterna(datoSd, valor, b, codificacion))
            mensaje = "Fichero escrito OK";
        else
            mensaje = "Error al escribir en el fichero";
        Toast.makeText(this, mensaje, Toast.LENGTH_SHORT).show();
    }

    private boolean puedeEscribir() {
        return miMemoria.disponibleEscritura();
    }

    private boolean tienePermiso() {
        return ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED;
    }

    private boolean obtienePermiso(int requestType) {
        if (!tienePermiso()) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, requestType);
            return false;
        }
        return true;
    }

    private void EsconderTeclados() {
        ((InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE)).hideSoftInputFromWindow(edDestino.getWindowToken(), 0);
        ((InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE)).hideSoftInputFromWindow(edNuevoNombre.getWindowToken(), 0);
    }

}
