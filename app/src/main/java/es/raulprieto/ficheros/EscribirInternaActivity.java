package es.raulprieto.ficheros;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class EscribirInternaActivity extends AppCompatActivity {
    public static final String NOMBREFICHERO = "resultado.txt";
    public static final String CODIGO = "UTF-8";
    Memoria miMemoria;

    @BindView(R.id.edTexto)
    EditText edTexto;
    @BindView(R.id.tvPropiedades)
    TextView tvPropiedades;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_escribir_interna);
        ButterKnife.bind(this);
        setTitle("Escribir Interna");

        miMemoria = new Memoria(getApplicationContext());
    }

    @OnClick(R.id.btGuardar)
    public void guardar() {
        if (miMemoria.escribirInterna(NOMBREFICHERO, edTexto.getText().toString(), false, CODIGO))
            tvPropiedades.setText(miMemoria.mostrarPropiedadesInterna(NOMBREFICHERO));
        else
            Toast.makeText(this, "No existe el fichero" + NOMBREFICHERO, Toast.LENGTH_SHORT).show();
        ((InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE)).hideSoftInputFromWindow(edTexto.getWindowToken(),0);
    }

}
