package es.raulprieto.ficheros;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
    }

    @OnClick({R.id.btEscribirInterna,R.id.btEscribirExterna,R.id.btLectura,R.id.btCodificacion,R.id.btExplorador})
    public void submit (View view)
    {
        Intent i = null;

        switch (view.getId()) {
            case R.id.btEscribirInterna:
                i = new Intent(this,EscribirInternaActivity.class);
                break;
            case R.id.btEscribirExterna:
                i = new Intent(this,EscribirExternaActivity.class);
                break;
            case R.id.btLectura:
                i = new Intent(this,LecturaActivity.class);
                break;
            case R.id.btCodificacion:
                i = new Intent(this,CodificacionActivity.class);
                break;
            case R.id.btExplorador:
                i = new Intent(this,ExploradorActivity.class);
                break;
        }
        startActivity(i);
    }
}
