package es.raulprieto.ficheros;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class LecturaActivity extends AppCompatActivity {
    static final int REQUEST_WRITE_OPERANDO = 1;
    static final int REQUEST_WRITE_RESULTADO = 2;
    static final int REQUEST_WRITE_OPERACION = 3;

    public final static String OPERACIONES = "operaciones_sd.txt";
    public final static String DATO = "dato.txt";
    public final static String DATO_SD = "dato_sd.txt";
    public final static String RESULTADO_SD = "resultado_sd.txt";
    public final static String VALOR = "valor.txt";
    public final static String NUMERO = "numero";    // No es numero.txt
    public final static String CODIFICACION = "UTF-8";

    Memoria miMemoria;
    Resultado resultado;
    @BindView(R.id.edRaw)
    EditText operando1;
    @BindView(R.id.edAsset)
    EditText operando2;
    @BindView(R.id.edInterna)
    EditText operando3;
    @BindView(R.id.edExterna)
    EditText operando4;
    @BindView(R.id.tvSuma)
    TextView tvSuma;
    @BindView(R.id.tvOperacion)
    TextView tvOperacion;

    @OnClick(R.id.btSuma)
    public void suma() {
        int cantidad = 0;
        String operaciones, mensaje, op1, op2, op3, op4;
        op1 = operando1.getText().toString();
        op2 = operando2.getText().toString();
        op3 = operando3.getText().toString();
        op4 = operando4.getText().toString();

        try {
            cantidad = Integer.parseInt(op1) + Integer.parseInt(op2) + Integer.parseInt(op3) + Integer.parseInt(op4);
            operaciones = String.format("--> "+Integer.parseInt(op1) + "+" + Integer.parseInt(op2) + "+" + Integer.parseInt(op3) + "+" + Integer.parseInt(op4) +" = " + cantidad + "\n");

            // Escribe y Pinta el resultado de la operación
            if (obtienePermiso(REQUEST_WRITE_RESULTADO) && puedeEscribir())
                escribir(RESULTADO_SD, Integer.toString(cantidad), false, CODIFICACION);
            extraerDatosMExterna(RESULTADO_SD, tvSuma);

            // Escribe y Pinta la operación
            if (obtienePermiso(REQUEST_WRITE_OPERACION) && puedeEscribir())
            escribir(OPERACIONES, operaciones, true, CODIFICACION);
            extraerDatosMExterna(OPERACIONES, tvOperacion);

        } catch (Exception e) {
            Log.e("Error", e.getMessage());
            cantidad = 0;
            tvSuma.setText(cantidad);
        } finally {
            EsconderTeclados();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lectura);
        ButterKnife.bind(this);

        iniciar();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case REQUEST_WRITE_OPERANDO:
                if (tienePermiso()) {
                    escribir(DATO_SD, getString(R.string.operandoMExterna), false, CODIFICACION);
                    extraerDatosMExterna(DATO_SD, operando4);
                } else
                    Toast.makeText(this, "No hay permisos para escribir en la m. Externa", Toast.LENGTH_SHORT).show();

                break;
            case REQUEST_WRITE_RESULTADO:
                if (tienePermiso()) {
                    escribir(RESULTADO_SD, tvSuma.getText().toString(), false, CODIFICACION);
                    extraerDatosMExterna(DATO_SD, tvSuma);
                } else
                    Toast.makeText(this, "No hay permisos para escribir en la m. Externa", Toast.LENGTH_SHORT).show();

                break;
            case REQUEST_WRITE_OPERACION:
                if (tienePermiso()) {
                    escribir(OPERACIONES, tvOperacion.getText().toString(), true, CODIFICACION);
                    extraerDatosMExterna(OPERACIONES, tvOperacion);
                } else
                    Toast.makeText(this, "No hay permisos para escribir en la m. Externa", Toast.LENGTH_SHORT).show();

                break;
        }
    }

    private void iniciar() {
        miMemoria = new Memoria(this);

        operandoRaw();

        operandoAsset();

        operandoMInterna();

        operandoMExterna();

    }

    private void operandoRaw() {
        resultado = miMemoria.leerRaw(NUMERO);
        if (resultado.getCodigo())
            operando1.setText(resultado.getContenido());
        else {
            operando1.setText("0");
            Toast.makeText(this, "Error al leer el raw" + NUMERO, Toast.LENGTH_SHORT).show();
        }
    }

    private void operandoAsset() {
        resultado = miMemoria.leerAsset(VALOR);
        if (resultado.getCodigo())
            operando2.setText(resultado.getContenido());
        else {
            operando2.setText("0");
            Toast.makeText(this, "Error al leer el asset" + VALOR, Toast.LENGTH_SHORT).show();
        }
    }

    private void operandoMInterna() {
        miMemoria.escribirInterna(DATO, getString(R.string.operandoMInterna), false, CODIFICACION);
        resultado = miMemoria.leerInterna(DATO, CODIFICACION);
        if (resultado.getCodigo())
            operando3.setText(resultado.getContenido());
        else {
            operando3.setText("0");
            Toast.makeText(this, "Error al leer la m. Interna" + DATO, Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * Escribe el dato en MExterna y Lee ese dato
     */
    private void operandoMExterna() {
        if (obtienePermiso(REQUEST_WRITE_OPERANDO) && puedeEscribir())
            escribir(DATO_SD, getString(R.string.operandoMExterna), false, CODIFICACION);
        extraerDatosMExterna(DATO_SD, operando4);
    }

    /**
     * Extrae un dato de un <b>fichero externo</b> para
     * pintarlo en un <b>TextView o EditText</b> de destino
     *
     * @param fichero Fichero contenedor del dato
     * @param destino TextView de destino
     */
    private void extraerDatosMExterna(String fichero, TextView destino) {
        resultado = miMemoria.leerExterna(fichero, CODIFICACION);
        if (resultado.getCodigo())
            destino.setText(resultado.getContenido());
        else {
            destino.setText("0");
            Toast.makeText(this, "Error al leer la m. Externa" + fichero, Toast.LENGTH_SHORT).show();
        }
    }

    private void escribir(String datoSd, String valor, boolean b, String codificacion) {
        String mensaje;
        if (miMemoria.escribirExterna(datoSd, valor, b, codificacion))
            mensaje = "Fichero escrito OK";
        else
            mensaje = "Error al escribir en el fichero";
        Toast.makeText(this, mensaje, Toast.LENGTH_SHORT).show();
    }

    private boolean puedeEscribir() {
        return miMemoria.disponibleEscritura();
    }

    private boolean tienePermiso() {
        return ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED;
    }

    private boolean obtienePermiso(int requestType) {
        if (!tienePermiso()) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, requestType);
            return false;
        }
        return true;
    }

    private void EsconderTeclados() {
        ((InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE)).hideSoftInputFromWindow(operando1.getWindowToken(), 0);
        ((InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE)).hideSoftInputFromWindow(operando2.getWindowToken(), 0);
        ((InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE)).hideSoftInputFromWindow(operando3.getWindowToken(), 0);
        ((InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE)).hideSoftInputFromWindow(operando4.getWindowToken(), 0);
    }

}
